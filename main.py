import requests, threading, hashlib, secrets, binascii, mnemonic, bip32utils
from bs4 import BeautifulSoup as bs
from multiprocessing.pool import ThreadPool as Pool

class Bip39Gen(object):
    def __init__(self, bip39wordlist):
        self.bip39wordlist = bip39wordlist
        word_count = 12
        checksum_bit_count = word_count // 3
        total_bit_count = word_count * 11
        generated_bit_count = total_bit_count - checksum_bit_count
        entropy = self.generate_entropy(generated_bit_count)
        entropy_hash = self.get_hash(entropy)
        indices = self.pick_words(entropy, entropy_hash, checksum_bit_count)
        self.print_words(indices)



    def generate_entropy(self, generated_bit_count):
        entropy = secrets.randbits(generated_bit_count)
        return self.int_to_padded_binary(entropy, generated_bit_count)


    def get_hash(self, entropy):
        generated_bit_count = len(entropy)
        generated_char_count = generated_bit_count // 4
        entropy_hex = self.binary_to_padded_hex(entropy, generated_char_count)

        entropy_hex_no_padding = entropy_hex[2:]

        entropy_bytearray = bytearray.fromhex(entropy_hex_no_padding)

        return hashlib.sha256(entropy_bytearray).hexdigest()


    def pick_words(self, entropy, entropy_hash, checksum_bit_count):
        checksum_char_count = checksum_bit_count // 4
        bit = entropy_hash[0:checksum_char_count]

        check_bit = int(bit, 16)
        checksum = self.int_to_padded_binary(check_bit, checksum_bit_count)

        source = str(entropy) + str(checksum)
        return [int(str('0b') + source[i:i + 11], 2) for i in range(0, len(source), 11)]


    def print_words(self, indices):
        words = [self.bip39wordlist[indices[i]] for i in range(len(indices))]
        word_string = ' '.join(words)
        self.mnemonic = word_string


    def int_to_padded_binary(self, num, padding):
        return bin(num)[2:].zfill(padding)


    def binary_to_padded_hex(self, bin, padding):
        num = int(bin, 2)
        return '0x{0:0{1}x}'.format(num, padding)
    
def main():
    while True:
            start()
            break

def getInternet():
    try:
        requests.get('http://1.1.1.1')
        return True
    except requests.ConnectionError:
        return False

lock = threading.Lock()

if getInternet() == True:
    bip_words = requests.get(
        'https://raw.githubusercontent.com/bitcoin/bips/master/bip-0039/english.txt').text.strip().split('\n')
else:
    pass

def getBalance(wallet):
    try:
        response = requests.get(f'https://bitcoinblockexplorers.com/address/{wallet}')
        soup = bs(response.text, 'lxml')
        balance = soup.find('div', {'class': "content"})
        x = 0
        for i in balance.find_all('span'):
            if x == 1:
                balance = i.string
                break
            if i.string == "Final Balance":
                x = 1
        if balance == None:
            getBalance(wallet)
        else:
            return balance
    except:
        getBalance(wallet)
        
def bip39(mnemonic_words):
    mobj = mnemonic.Mnemonic("english")
    seed = mobj.to_seed(mnemonic_words)

    bip32_root_key_obj = bip32utils.BIP32Key.fromEntropy(seed)
    bip32_child_key_obj = bip32_root_key_obj.ChildKey(
        44 + bip32utils.BIP32_HARDEN
    ).ChildKey(
        0 + bip32utils.BIP32_HARDEN
    ).ChildKey(
        0 + bip32utils.BIP32_HARDEN
    ).ChildKey(0).ChildKey(0)

    return bip32_child_key_obj.Address()
        
def check():
    while True:
        words = Bip39Gen(bip_words).mnemonic
        wallet = bip39(words)
        balance = getBalance(wallet)
        with lock:
            if balance != None:
                print(f'Balance: {balance} Address: {wallet}')
        if balance != "0 BTC" and balance != None:
            pass
            with open('results/money.txt', 'a') as w:
                w.write(
                    f'Address: {wallet} | Balance: {balance} | Mnemonic phrase: {words}\n')

def start():
    threads = 100
    if getInternet() == True:
        pool = Pool(threads)
        for _ in range(threads):
            pool.apply_async(check, ())
        pool.close()
        pool.join()
    else:
        print("Can't connect to Network")
        exit()

if __name__ == '__main__':
    main()
